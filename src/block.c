#include "allocator.h"
#include "libft.h"

static t_block	*add_to_sblock(t_sblock *cur_sblock, size_t size)
{
	size_t offset = (sizeof(t_sblock) + 15) & ~15;
	t_block *block = (t_block *)((char *)cur_sblock + offset);

	while (block->next != 0)
	{
		if ((block->freed) && (size <= block->size))
		{
			// TODO: handle empty space left here
			block->freed = 0;
			block->size = size;
			return (block);
		}
		block = block->next;
	}
	// not enough space in freed blocks, need a new one
	t_block *new_block = (t_block *)((char *)block + block->size + block->meta);
	// offset to return aligned pointer
	uintptr_t ptr = (uintptr_t)new_block;
	uintptr_t aligned_ptr = (ptr + sizeof(t_block) + 15) & ~15;

	if ((uintptr_t)((char*)new_block + size) + aligned_ptr - ptr > (uintptr_t)((char *)cur_sblock + cur_sblock->size))
		return (NULL);
	new_block->magic = 0x42424242;
	new_block->freed = 0;
	new_block->size = size;
	new_block->meta = aligned_ptr - ptr;
	new_block->next = NULL;
	block->next = new_block;
	return (new_block);
}

static int		sblock_has_size(t_sblock *cur_sblock, size_t size)
{
	size_t offset = (sizeof(t_sblock) + 15) & ~15;
	t_block *block = (t_block *)((char *)cur_sblock + offset);
	// can be more
	size_t min_size = size + ((sizeof(t_block) + 15) & ~15); 
	size_t total_size = 0;

	while (block != 0)
	{
		if (!block->freed)
			total_size += block->size + block->meta;
		block = block->next;
	}
	if (min_size > cur_sblock->size - total_size)
		return (0);
	// at this point it is not guaranteed that we can add a block
	// just that we are pretty sure this sblock cannot host a block
	return (1);
}

t_block			*add_block(size_t size)
{
	if (!size)
		return ((void *)0);
	t_sbsize sbsize;
	get_memsize(size, &sbsize);
	size_t sblock_offset = (sizeof(t_sblock) + 15) & ~15;
	
	// select sblock
	if (!g_heap)
	{
		void *ret = new_superblock(size, sbsize);
		if (!ret)
			return ((void *)0);
		return (t_block *)((char *)ret + sblock_offset);
	}
	else
	{
		t_sblock *cur_sblock = (t_sblock *)g_heap;
		while (cur_sblock)
		{
			if (sblock_has_size(cur_sblock, size))
			{
				t_block *block = add_to_sblock(cur_sblock, size); 
				if (block)
					return (block);
			}
			cur_sblock = cur_sblock->next;
		}
		if (!cur_sblock)
		{
			void *ret = new_superblock(size, sbsize);
			if (!ret)
				return ((void *)0);
			return (t_block *)((char *)ret + sblock_offset);
		}
	}
	return ((void *)0);
}

int		remove_block(void *ptr)
{
	t_sblock *cur_sblock = (t_sblock *)g_heap;	
	t_block *tmp_block = 0;
	size_t sblock_offset = (sizeof(t_sblock) + 15) & ~15;
	if (!cur_sblock)
		return (FAILED);
	while (cur_sblock)
	{		
		tmp_block = (t_block *)((char *)cur_sblock + sblock_offset);
		while (tmp_block)
		{
			if (((char *)tmp_block + tmp_block->meta) == ptr)
			{
				tmp_block->freed = 1;
				return (SUCCESS);
			}
			tmp_block = tmp_block->next;
		}
		cur_sblock = cur_sblock->next;
	}
	return (FAILED);
}

void	merge_empty_blocks(void)
{
	t_sblock *cur_sblock = (t_sblock *)g_heap;
	t_block *tmp_block = 0;
	t_block *new = 0;
	size_t size = 0;
	size_t sblock_offset = (sizeof(t_sblock) + 15) & ~15;

	if (!cur_sblock)
		return ;
	while (cur_sblock)
	{
		tmp_block = (t_block *)((char *)cur_sblock + sblock_offset);
		while (tmp_block)
		{
			new = tmp_block;
			size = 0;
#ifdef ASSERTIONS			
			if (tmp_block->magic != 0x42424242)
			{
				ft_putendl("corruption detected");
				abort();
			}
#endif
			if (tmp_block->freed == 1)
			{
				while (tmp_block->freed == 1)
				{
					if (!tmp_block->next)
						break ;
					size += tmp_block->size;
					tmp_block = tmp_block->next;
				}
				if (tmp_block->next == 0)
				{
					if (tmp_block->freed == 1)
					{
						new->next = 0;
						new->size = tmp_block->size + size;
						return ;
					}
					else
					{
						new->next = tmp_block;
						new->size = size;
						return ;
					}
				}
				else
				{
					new->next = tmp_block;
					new->size = size;
				}
			}
			tmp_block = tmp_block->next;
		}
		cur_sblock = cur_sblock->next;
	}
}
