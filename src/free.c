#include "allocator.h"
#include "libft.h"

static int ptr_is_alloc(void *ptr)
{
	t_sblock *cur_sblock = (t_sblock *)g_heap;
	t_block *tmp_block = 0;
	size_t sblock_offset = (sizeof(t_sblock) + 15) & ~15;
	while (cur_sblock)
	{
		tmp_block = (t_block *)((char *)cur_sblock + sblock_offset);
		while (tmp_block)
		{
			if (((char *)tmp_block + tmp_block->meta) == ptr)
				return (1);
			tmp_block = tmp_block->next;
		}
		cur_sblock = cur_sblock->next;
	}
	return (0);
}

void	free(void *ptr)
{
	if (!ptr)
		return ;
	// check if ptr was allocated
	if (!ptr_is_alloc(ptr))
		return ;
	int ret = remove_block(ptr);
	if (ret == FAILED)
		return ;
	unmap_empty_sblocks();
	merge_empty_blocks();
}
