#include "allocator.h"
#include "libft.h"

void	*realloc(void *ptr, size_t size)
{
	if (!ptr)
	{
		void *ret = malloc(size);
		if (!ret)
			return ((void *)0);
		return (ret);
	}
	if (size == 0)
	{
		free(ptr);
		return NULL;
	}
	// check previous alloc size
	t_sblock *cur_sblock = (t_sblock *)g_heap;
	t_block *tmp_block = 0;
	size_t sblock_offset = (sizeof(t_sblock) + 15) & ~15;
	size_t old_size = 0;
	while (cur_sblock)
	{
		tmp_block = (t_block *)((char *)cur_sblock + sblock_offset);
		while (tmp_block != 0)
		{
			if (((char *)tmp_block + tmp_block->meta) == ptr)
			{
				old_size = tmp_block->size;
				break;
			}
			tmp_block = tmp_block->next;
		}
		if (old_size != 0)
			break ;
		cur_sblock = cur_sblock->next;
	}
	if (!cur_sblock)
		return (NULL); // UB
	void *new = malloc(size);
	if (!new)
		return ((void *)0);
	if (size < old_size)
		old_size = size;
	void *ret = ft_memcpy(new, ptr, old_size);
	if (!ret)
		return (NULL);
	free(ptr);
	return (ret);
}
