#include "allocator.h"
#include "libft.h"

void	*malloc(size_t size)
{
	if (size <= 0)
		return ((void *)0);

	t_block *block = add_block(size);
	if (!block)
		return ((void *)0);
#ifdef ASSERTIONS
	if  ((uintptr_t)((char *)block + block->meta) % 16 != 0)
	{
		ft_putendl("not aligned! no good");
		ft_putptr((uintptr_t) (t_block *)((char *)block + block->meta));
		abort();
	}
#endif
	return (t_block *)((char *)block + block->meta);
}
