#include "allocator.h"
#include "libft.h"
#include <string.h>

t_sblock	*g_heap = NULL;

size_t		get_memsize(size_t size, t_sbsize *sbsize)
{
	size_t page_size = (size_t)getpagesize();
	if (size <=  page_size)
	{
		*sbsize = TINY;
		return (TINY_NB_BLOCKS * page_size);
	}
	else if ((size > page_size) && (size <= page_size * 256))
	{
		*sbsize = SMALL;
		return (SMALL_NB_BLOCKS * page_size);
	}
	*sbsize = LARGE;
	return (size);
}

char	*str_from_sbsize(t_sbsize sbsize)
{
	if (sbsize == TINY)
		return ("TINY");
	else if (sbsize == SMALL)
		return ("SMALL");
	else if (sbsize == LARGE)
		return ("LARGE");
	// should never happen
	abort();
}

void		show_alloc_mem(void)
{
	t_sblock *new_sblock = (t_sblock *)g_heap;
	t_block *block = 0;
	int size = 0;	
	size_t sblock_offset = (sizeof(t_sblock) + 15) & ~15;
	if (new_sblock)
	{
		while (new_sblock)
		{
			ft_putstr(str_from_sbsize(new_sblock->sbsize));
 			ft_putstr(" : ");
			ft_putptr((uintptr_t)new_sblock);
			ft_putstr("\n");
			block = (t_block *)((char *)new_sblock + sblock_offset);
			while (block)
			{
				if (!block->freed)
				{
					ft_putptr((uintptr_t)((char *)block + block->meta));
					ft_putstr(" - ");
					ft_putptr((uintptr_t)((char *)block + block->meta + block->size));
					ft_putstr(" : ");
					ft_putnbr(block->size);
					ft_putendl(" octets");
					size += (int)block->size;
				}
				block = block->next;
			}
			new_sblock = new_sblock->next;
		}
		ft_putstr("Total : ");
		ft_putnbr(size);
		ft_putendl(" octets");
	}
}
