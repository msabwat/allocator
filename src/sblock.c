#include "allocator.h"
#include "libft.h"
#include <stdio.h>
#include <sys/resource.h>

t_sblock		*new_superblock(size_t size, t_sbsize sbsize)
{
	size_t mem_size = 0;

	t_sblock *sblock = (t_sblock *)g_heap;
	size_t page_size = getpagesize();
	if (sbsize == TINY)
		mem_size = TINY_NB_BLOCKS * page_size;
	else if (sbsize == SMALL)
		mem_size = SMALL_NB_BLOCKS * page_size;
	else
	{
		size_t new_size = size / page_size;
		mem_size = (new_size + 1) * page_size;
	}
	size_t offset = (sizeof(t_sblock) + 15) & ~15;
	struct rlimit limit;

	if (getrlimit(RLIMIT_AS, &limit) == 0)
	{
		if ((mem_size + offset > limit.rlim_cur) ||
			(mem_size + offset > limit.rlim_max))
		{
#ifdef ASSERTIONS
			ft_putstr("requesting more rlimit for the current process ");
			ft_putstr(" soft : ");
			ft_putnbr((int) limit.rlim_cur);
			ft_putstr(" hard : ");
			ft_putnbr((int) limit.rlim_max);
			ft_putchar('\n');
#endif
			return (NULL);
		}
	}
	else
	{
		// yolo (if you can't rely on getrlimit(), might as well try)
		(void)0;
#ifdef ASSERTIONS
		ft_putendl("could not get rlimit, something bad can happen\n");
		abort();
#endif
	}
	size_t mmap_size = mem_size + offset + (offset % 4096);
	void *p_sblock = mmap(NULL, mmap_size,
						  PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANON, -1, 0);
	if (p_sblock == MAP_FAILED)
		return (NULL);
	t_sblock *new_sblock = (t_sblock *)p_sblock;
	new_sblock->sbsize = sbsize;
	new_sblock->size = mmap_size;
	new_sblock->next = NULL;
	t_block *block = (t_block *)((char *)new_sblock + offset);
	block->magic = 0x42424242;
	block->freed = 0;
	// [padding] - [data]
	// meta+align   ^ malloc pointer aligned address
	block->meta = (sizeof(t_block) + 15) & ~15;
	block->size = size;
	block->next = NULL;
	if (!g_heap)
		g_heap = p_sblock;
	else
	{
		while (sblock->next != 0)
			sblock = sblock->next;
		sblock->next = new_sblock;
	}
	return (t_sblock *)(new_sblock);
}

static int	is_unused_sblock(t_sblock *sblock)
{
	size_t offset = (sizeof(t_sblock) + 15) & ~15;
	t_block *tmp_block = (t_block *)((char *)sblock + offset);
	if (tmp_block == 0)
		return (1);
	if (!sblock)
		return (0);
	while (tmp_block)
	{
		if (tmp_block->freed == 0)
			return (0);
		tmp_block = tmp_block->next;
	}
	return (1);
}

int			unmap_empty_sblocks(void)
{
	t_sblock *cur_sblock = (t_sblock *)g_heap;
	if (!cur_sblock)
		return (FAILED);
	if (cur_sblock->next == 0)
		// nothing to do if there is one block
		return (SUCCESS);
	while (cur_sblock)
	{
		if (is_unused_sblock(cur_sblock->next))
		{
			t_sblock *to_free = cur_sblock->next;
			if (cur_sblock->next->next == 0)
				cur_sblock->next = 0;
			else
				cur_sblock->next = cur_sblock->next->next;
			int ret = munmap(to_free, to_free->size);
			if (ret == -1)
				return (FAILED);
		}
		cur_sblock = cur_sblock->next;
	}
	return (SUCCESS);
}
