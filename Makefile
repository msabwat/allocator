
TARGET=libft_malloc_$(HOSTTYPE).so

ifeq ($(HOSTTYPE),)
	HOSTTYPE := $(shell uname -m)-$(shell uname -s)
endif

override CFLAGS := -Wall -Wextra -Werror -fPIC -DASSERTIONS -g $(CFLAGS)

CC=clang

INC=-I.

LIBFT_INC=-Ilibft/

SRC_NAME= 	malloc.c \
			free.c \
			realloc.c \
			block.c sblock.c \
			utils.c

OBJ_NAME=$(SRC_NAME:.c=.o)

SRC_PATH=src

OBJ_PATH=.obj

SRC=$(addprefix $(SRC_PATH)/,$(SRC_NAME))

OBJS=$(addprefix $(OBJ_PATH)/,$(OBJ_NAME))

all: makedir $(TARGET)

makedir:
	@mkdir -p .obj

test: makedir $(TARGET)
	$(CC) $(CFLAGS) $(LIBFT_INC) $(INC) -L. -lft_malloc test/test.c -L libft/ -lft -o tester1
	$(CC) $(CFLAGS) $(LIBFT_INC) $(INC) -L. -lft_malloc test/main.c -L libft/ -lft -o tester2
$(TARGET): $(OBJS)
	$(MAKE) $(TEST) -C libft
	$(CC) $(CFLAGS) -shared $(OBJS) $(LIBFT_INC) $(INC) -L libft/ -lft -o $(TARGET)
	rm -fr libft_malloc.so && ln -s libft_malloc_$(HOSTTYPE).so libft_malloc.so
$(OBJ_PATH)/%.o: $(SRC_PATH)/%.c
	$(CC) $(CFLAGS) $(LIBFT_INC) $(INC) -c $< -o $@

clean:
	rm -fr $(TARGET) libft_malloc.so tester1 tester2

fclean: clean
	rm -fr $(OBJ_PATH)
	$(MAKE) fclean -C libft

re: fclean all
