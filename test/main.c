#include "allocator.h"
#include "libft.h"
#include <assert.h>

int main(int ac, char **av)
{
	int fd = 1;
	int ret = 0;
	char *str;
	void *ptr;
	char **args;
	while (ret != get_next_line(fd, &str))
	{
		args = ft_strsplit(str, ' ');
		free(str);
		assert(args);
		if (ft_strcmp(args[0], "alloc") == 0)
		{
			ft_putendl("##### alloc #####");
			ptr = malloc(atol(args[1]));
			assert(ptr);
			ft_putendl("##### memory pages :#####");
		}
		else if (ft_strcmp(args[0], "free") == 0)
		{
			ft_putendl("##### free last alloc #####");
			free(ptr);
			ft_putendl("##### memory pages :#####");
		}
		else if (ft_strcmp(args[0], "realloc") == 0)
		{
			ft_putendl("##### realloc previous alloc #####");
			ptr = realloc(ptr, atol(args[1]));
			ft_putendl("##### memory pages :#####");
		}
		show_alloc_mem();
	}
	(void)ac, (void)av;
	return (0);
}
