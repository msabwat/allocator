#include <stddef.h>

#define TINY_NB_BLOCKS 64
#define SMALL_NB_BLOCKS 256

#define SUCCESS 0
#define FAILED 1

#include <sys/mman.h>
#include <unistd.h>
#include <stdint.h>
#include <stdbool.h>

typedef enum e_sbsize
{
	TINY,
	SMALL,
	LARGE,
}			t_sbsize;

typedef struct s_block
{
	int			magic;
	bool			freed;
	size_t			size;
	size_t			meta;
	struct s_block	*next;
}			t_block;

typedef struct s_sblock
{
	t_sbsize 		sbsize;
	size_t			size;
	struct s_sblock	*next;
}			t_sblock;

extern t_sblock			*g_heap;

void		show_alloc_mem(void);
void		free(void *ptr);
void		*malloc(size_t size);
void		*realloc(void  *ptr, size_t size);

// internal functions
size_t		get_memsize(size_t size, t_sbsize *sbsize);
char		*str_from_sbsize(t_sbsize sbsize);
t_sblock	*new_superblock(size_t size, t_sbsize sbsize);
int		unmap_empty_sblocks(void);

t_block		*add_block(size_t size);
int			remove_block(void *block);
void		merge_empty_blocks(void);
