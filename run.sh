#!/bin/sh

if [[ "$OSTYPE" == "linux-gnu"* ]]; then
    export LD_PRELOAD="./libft_malloc.so"
	export LD_LIBRARY_PATH="."
elif [[ "$OSTYPE" == "darwin"* ]]; then
    # Mac OSX
    export DYLD_INSERT_LIBRARIES="./libft_malloc.so"
    export DYLD_FORCE_FLAT_NAMESPACE=1
else
    echo "$OS_TYPE not supported"
    exit 1
fi
$@
